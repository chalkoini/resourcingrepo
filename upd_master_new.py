import pandas as pd
import time
import os
from datetime import date, timedelta

##----------------------------------------------------------------------------------------##
##------------------------------  Get Openings/Resources DF  -----------------------------##
##----------------------------------------------------------------------------------------##
from opening_xml_parser import openings_df
from resources_xml_parser import resources_df

## Start timer for execution time check
start_time = time.time()

## Current folder
current_folder = os.getcwd()
output_folder = current_folder + '\\Output\\'
if not os.path.isdir(output_folder):
    os.makedirs(output_folder)

allocation_file = output_folder + "\\Allocation_List.csv"
unallocated_resources_file = output_folder + "\\Unallocated_Resources.csv"
unallocated_openings_file = output_folder + "\\Unallocated_Openings.csv"

## config properties
pd.options.mode.chained_assignment = None
score_threshold = 1.5
global total_score
total_score = 0

##----------------------------------------------------------------------------------------##
##---------------------------------  Final Match DF columns  -----------------------------##
##----------------------------------------------------------------------------------------##
match_df_column_list = ['match_id', 'resource_id', 'opening_id', 'project_key', 'score', 'key_project',
                        'key_position', 'project_start_date', 'project_end_date', 'is_allocated']

##----------------------------------------------------------------------------------------##
##---------------------------------  Function Declarations  ------------------------------##
##----------------------------------------------------------------------------------------##
## Function to start processing on a key project - match and allocate resources
def process_a_key_project(project):
    project_match_df = get_project_match_and_score(project)

    is_balancing_required = check_resource_balancing(project_match_df)
    if is_balancing_required:
        balanced_allocation_for_project(project_match_df)
    else:
        direct_project_allocation(project_match_df)

    return project_match_df

## Function to start processing on a non-key project - match and allocate resources
def process_a_non_key_project(project):
    project_match_df = get_project_match_and_score(project)

    project_match_df = direct_project_allocation(project_match_df)

    return project_match_df

## get all matches and scores for a project
def get_project_match_and_score(project):
    is_key_project = openings_df[openings_df['ProjectKey'] == project]['IsKeyProject'].unique()[0]
    project_match_df = pd.DataFrame(data=None, columns=None, index=None)
    for index, opening in openings_df[openings_df['ProjectKey'] == project].iterrows():

        resource_match_df = resources_df[(resources_df['AvailableFromDate'] <= opening['RequestStartDate']) &
                                         (resources_df['Skills'].apply(
                                             lambda x: set(x) >= set(opening['MandatorySkills'])))]

        if not resource_match_df.empty:
            # Calculate Score for all resources against an opening
            resource_match_df = calculate_score_of_resource(resource_match_df, opening)

            # Validate the score calculated
            if ((is_key_project == 'Y') & (opening['IsKeyPosition'] == 'N')):
                resource_match_df = resource_match_df[resource_match_df['score'] <= 1.3]
            elif (is_key_project == 'N') & (opening['IsKeyPosition'] == 'Y'):
                resource_match_df = resource_match_df[resource_match_df['score'] <= 1.5]
            elif (is_key_project == 'N') & (opening['IsKeyPosition'] == 'N'):
                resource_match_df = resource_match_df[resource_match_df['score'] <= 1]

            final_opening_valid_match_df = populate_df_for_resources(resource_match_df, opening)

            project_match_df = pd.concat([project_match_df, final_opening_valid_match_df])

    project_match_df = project_match_df.reset_index(drop=True)
    project_match_df['match_id'] = project_match_df.index

    return project_match_df

## Function to calculate score of the resource against matched openings in function match_resource_with_openings_df_PR
def calculate_score_of_resource(resource_match_df, opening):
    resource_match_df['score'] = 1

    DomainExperienceMatched = resource_match_df['DomainExperience'].apply(lambda x: opening['ProjectDomain'] in set(x))
    CustomerMatched = resource_match_df['PreviousCustomerExperience'].apply(lambda x: opening['CustomerName'] in set(x))

    DomainExperienceMatchedScore = DomainExperienceMatched.apply(lambda x: 0.2 if (x) else 0)
    CustomerMatchedScore = CustomerMatched.apply(lambda x: 0.3 if (x) else 0)
    resource_match_df['score'] += DomainExperienceMatchedScore + CustomerMatchedScore

    # Score Calculation   for Years of work experience
    workex_diff = opening['YearsOfExperience'] - resource_match_df['YearsOfExperience']
    workex_score = workex_diff.apply(lambda x: x * 0.05 if (x < 0) else 0)
    resource_match_df['score'] += workex_score

    nagp_score = resource_match_df['NAGP'].apply(lambda x: 0.3 if (x == 'Y') else 0)
    rating_score = resource_match_df['Rating'].apply(lambda x: 0.2 if (x == 'A+') else (0.1 if (x == 'A') else 0))
    resource_match_df['score'] += nagp_score + rating_score

    return resource_match_df

## Function to populate a match dataframe for all resources against an opening
def populate_df_for_resources(resource_match_df, opening):
    matched_records_for_opening = pd.DataFrame(data=None, columns= match_df_column_list, index=None)

    matched_records_for_opening['score'] = resource_match_df['score']
    matched_records_for_opening['key_project'] = opening['IsKeyProject']
    matched_records_for_opening['key_position'] = opening['IsKeyPosition']
    matched_records_for_opening['project_start_date'] = opening['RequestStartDate']
    matched_records_for_opening['project_end_date'] = opening['ProjectEndDate']
    matched_records_for_opening['is_allocated'] = 'N'
    matched_records_for_opening['resource_id'] = resource_match_df['EmployeeID']
    matched_records_for_opening['opening_id'] = opening['RequestID']
    matched_records_for_opening['match_id'] = resource_match_df.index
    matched_records_for_opening['project_key'] = opening['ProjectKey']

    return matched_records_for_opening

## Check if balancing is required in a key project
def check_resource_balancing(project_match_df):
    is_balancing_required = True
    openings_matched_list = project_match_df['opening_id'].unique()
    kp_nkp_df = (openings_df[openings_df['RequestID'].isin(openings_matched_list)]['IsKeyPosition']).to_frame()
    kp_cnt = ((kp_nkp_df[kp_nkp_df['IsKeyPosition'] == 'Y']['IsKeyPosition']).value_counts()).tolist()
    nkp_cnt = ((kp_nkp_df[kp_nkp_df['IsKeyPosition'] == 'N']['IsKeyPosition']).value_counts()).tolist()

    if kp_cnt == []:
        kp_cnt = 0
    else:
        kp_cnt = kp_cnt[0]

    if nkp_cnt == []:
        nkp_cnt = 0
    else:
        nkp_cnt = nkp_cnt[0]

    if nkp_cnt >= (3 * kp_cnt):
        is_balancing_required = False

    return is_balancing_required

def balanced_allocation_for_project(project_match_df):

    # Sort dataframe on score in asc order so that opening with highest score comes first (to get non key openings)
    project_match_df.sort_values(['score'], inplace=True, ascending=True)
    non_key_positions_openings_list = project_match_df[project_match_df['key_position'] == 'N']['opening_id'].unique()

    #Sort dataframe on score in desc order so that opening with highest score comes first (to get key openings)
    project_match_df.sort_values(['score'], inplace=True, ascending=False)
    key_positions_openings_list = project_match_df[project_match_df['key_position'] == 'Y']['opening_id'].unique()

    if ((len(key_positions_openings_list) == 0) and (len(non_key_positions_openings_list) != 0)):
        direct_project_allocation(project_match_df)
    elif (len(key_positions_openings_list) != 0):
        opening_cnt = 0
        ko_hs_sum = 0
        # Check if all key openings highest score avg is less than equal to 1.5, if yes can directly match all highest score unique resources
        for ko in key_positions_openings_list:
            ko_hs_row = project_match_df.ix[project_match_df[project_match_df['opening_id'] == ko]['score'].idxmax()]
            ko_hs_sum += ko_hs_row['score']
            opening_cnt += 1
        if (ko_hs_sum/opening_cnt <= 1.5):
            direct_project_allocation(project_match_df)
        else:
            # Check if all openings highest score avg is less than equal to 1.5, if yes can directly match all highest score unique resources
            for nko in non_key_positions_openings_list:
                nko_hs_row = project_match_df.ix[
                    project_match_df[project_match_df['opening_id'] == nko]['score'].idxmax()]
                ko_hs_sum += nko_hs_row['score']
                opening_cnt += 1

            if (ko_hs_sum / opening_cnt <= 1.5):
                direct_project_allocation(project_match_df)
            else:
                # Balance the openings highest score matches with low score matches
                opening_match_above_threshold = project_match_df[project_match_df['score'] > 1.5]['opening_id'].unique().tolist()
                opening_match_below_threshold = project_match_df[project_match_df['score'] <= 1.5]['opening_id'].unique().tolist()

                # Allocate openings which hav only one resource allocated to them directly - can we do it ???
                opening_match_cnt_dict = (project_match_df['opening_id'].value_counts().to_dict())
                opening_match_cnt_dict_list = sorted(opening_match_cnt_dict, key=opening_match_cnt_dict.get)

                opening_with_one_match = []
                for key, value in opening_match_cnt_dict.items():
                    if value == 1:
                        opening_with_one_match.append(key)

                for opening in opening_with_one_match:
                    project_match_df = allocate_resource(project_match_df[project_match_df['opening_id'] == opening].iloc[0]['match_id'], project_match_df)
                    try:
                        opening_match_below_threshold.remove(opening)
                        opening_match_above_threshold.remove(opening)
                    except ValueError:
                        pass
                direct_allocate_opening = False
                # Idea is to pick a high score match and try to negate it with low score matches, please follow below nomenclature
                # above means matches above 1.5 score
                # below means matches below 1.5 score
                for above_opening in opening_match_above_threshold:
                    project_match_df_temp = project_match_df
                    # Check if below matches are all allocated or the below 1.5 match aren't there
                    if opening_match_below_threshold != []:
                        below_match_found = False
                        break_score = 2
                        # This will iterate over above matches from 2 to 1.5 till a match to negate id foound in below
                        while (break_score > 1.5):
                            above_opening_valid_df = project_match_df_temp[(project_match_df_temp['opening_id'] == above_opening) &
                                                                                          (project_match_df_temp['is_allocated'] == 'N')]
                            if not above_opening_valid_df.empty:
                                above_hs_row = above_opening_valid_df.ix[above_opening_valid_df['score'].idxmax()]
                            else:
                                break
                            project_match_df_temp = project_match_df_temp[project_match_df_temp['match_id'] != above_hs_row['match_id']]
                            break_score = above_hs_row['score']
                            if (above_hs_row['score'] > 1.5):
                                score_to_negate = above_hs_row['score'] - score_threshold
                                max_score_allowed_for_below = score_threshold - score_to_negate
                                try:
                                    opening_match_below_threshold_temp = opening_match_below_threshold
                                    opening_match_below_threshold_temp.remove(above_opening)
                                except ValueError:
                                    pass
                                below_match_df = project_match_df[(project_match_df['opening_id'].isin(opening_match_below_threshold_temp)) &
                                                                  (project_match_df['resource_id'] != above_hs_row['resource_id']) &
                                                                  (project_match_df['is_allocated'] == 'N') &
                                                                  (project_match_df['score'] <= max_score_allowed_for_below)]
                                if not below_match_df.empty:
                                    below_match_found = True
                                    project_match_df = allocate_resource(below_match_df.ix[below_match_df['score'].idxmax()]['match_id'] , project_match_df)
                                    global total_score
                                    total_score += below_match_df.ix[below_match_df['score'].idxmax()]['score']
                                    try:
                                        opening_match_below_threshold.remove(below_match_df.ix[below_match_df['score'].idxmax()]['opening_id'])
                                        opening_match_above_threshold.remove(below_match_df.ix[below_match_df['score'].idxmax()]['opening_id'])
                                    except ValueError:
                                        pass

                                if below_match_found:
                                    project_match_df = allocate_resource(above_hs_row['match_id'], project_match_df)
                                    global total_score
                                    total_score += above_hs_row['score']
                                    # opening_match_above_threshold.remove(above_opening)
                                    try:
                                        opening_match_below_threshold.remove(above_opening)
                                    except ValueError:
                                        pass
                                    break_score = 0
                            else:
                                direct_allocate_opening = True
                                break_score = 0
                        # This means either project has no opening for which a match has highest score greater than 1.5 or no match is able to negate the high score opening match
                        if direct_allocate_opening:
                            df_to_be_processed = project_match_df[
                                (project_match_df['opening_id'] == above_opening) &
                                (project_match_df['is_allocated'] == 'N') &
                                (project_match_df['score'] <= 1.5)]
                            if not df_to_be_processed.empty:
                                project_match_df = allocate_resource(
                                    df_to_be_processed.ix[df_to_be_processed['score'].idxmax()]['match_id'], project_match_df)
                                global total_score
                                total_score += df_to_be_processed.ix[df_to_be_processed['score'].idxmax()]['score']
                                try:
                                    opening_match_above_threshold.remove(above_opening)
                                    opening_match_below_threshold.remove(above_opening)
                                except ValueError:
                                    pass
                    else:
                        df_to_be_processed = project_match_df[
                            (project_match_df['opening_id'].isin(opening_match_above_threshold)) &
                            (project_match_df['is_allocated'] == 'N') &
                            (project_match_df['score'] <= 1.5)]
                        if not df_to_be_processed.empty:
                            project_match_df = direct_allocation_for_key_position(df_to_be_processed, project_match_df)

                df_to_be_processed = project_match_df[
                    (project_match_df['opening_id'].isin(opening_match_below_threshold)) &
                    (project_match_df['is_allocated'] == 'N') &
                    (project_match_df['score'] <= 1.5)]

                if not df_to_be_processed.empty:
                    project_match_df = direct_allocation_for_key_position(df_to_be_processed, project_match_df)

## Allocated a resource in a project
def allocate_resource(match_id, project_match_df):
    project_match_df.loc[(project_match_df['match_id'] == match_id), 'is_allocated'] = 'Y'
    matched_rows = project_match_df[project_match_df['match_id'] == match_id]
    resource_dict = dict(zip(matched_rows['resource_id'], matched_rows['project_end_date']))

    for key, value in resource_dict.items():
        project_match_df.loc[((project_match_df['resource_id'] == key) &
                              (project_match_df['is_allocated'] != 'Y')), 'is_allocated'] = 'AA'

    return project_match_df


def direct_project_allocation(project_match_df):
    project_score = 0
    try:
        matched_openings_dict = (project_match_df['opening_id'].value_counts().to_dict())
    except KeyError:
        matched_openings_dict = {}
    matched_openings_list = sorted(matched_openings_dict, key=matched_openings_dict.get)
    for opening in matched_openings_list:
        selected_df_row = project_match_df[(project_match_df['is_allocated'] == 'N') & (project_match_df['opening_id'] == opening)]
        if not selected_df_row.empty:
            selected_df_row = selected_df_row.ix[selected_df_row['score'].idxmax()]
            project_score += selected_df_row['score']
            match_id = selected_df_row['match_id']
            resource_id = selected_df_row['resource_id']
            next_avail_date = selected_df_row['project_end_date']
            project_match_df = allocate_resource(match_id, project_match_df)
            update_resource_df_for_date(resource_id, next_avail_date)
    global total_score
    total_score += project_score
    return project_match_df

def direct_allocation_for_key_position(df_to_be_processed, project_match_df):
    project_score = 0
    try:
        matched_openings_dict = (df_to_be_processed['opening_id'].value_counts().to_dict())
    except KeyError:
        matched_openings_dict = {}
    # matched_openings_dict = (df_to_be_processed['opening_id'].value_counts().to_dict())
    matched_openings_list = sorted(matched_openings_dict, key=matched_openings_dict.get)
    for opening in matched_openings_list:
        selected_df_row = project_match_df[
            (project_match_df['is_allocated'] == 'N') & (project_match_df['opening_id'] == opening)]
        if not selected_df_row.empty:
            selected_df_row = selected_df_row.ix[selected_df_row['score'].idxmax()]
            project_score += selected_df_row['score']
            match_id = selected_df_row['match_id']
            resource_id = selected_df_row['resource_id']
            next_avail_date = selected_df_row['project_end_date']
            project_match_df = allocate_resource(match_id, project_match_df)
            update_resource_df_for_date(resource_id, next_avail_date)
    global total_score
    total_score += project_score
    return project_match_df

def update_resource_df_for_date(resource_id, next_avail_date):
    next_avail_date_fmt = time.strptime(next_avail_date, '%Y%m%d')
    next_avail_date_new = date(next_avail_date_fmt.tm_year, next_avail_date_fmt.tm_mon, next_avail_date_fmt.tm_mday) + timedelta(1)
    resources_df.loc[(resources_df['EmployeeID'] == resource_id), 'AvailableFromDate'] = str(next_avail_date_new.strftime('%Y%m%d'))

##----------------------------------------------------------------------------------------##
##---------------------------  Processing starts from here  ------------------------------##
##----------------------------------------------------------------------------------------##
print ('************************************************************************************')
print ('***************************** Resourcing Problem ***********************************')
print ('************************************************************************************')
print ('Execution Starts')
print('Total number of openings: ' + str(len(openings_df.index)))
print('Total number of resourcess: ' + str(len(resources_df.index)))
print ('Matching resources with openings..')
final_match_df_for_all_projects = pd.DataFrame(data=None, columns=match_df_column_list, index=None)

# Sort openings on request start date so that openings with earlier start date are allocated first
openings_df.sort_values(['RequestStartDate'], inplace=True, ascending=True)

# Create list of key and non key projects
key_project_list = openings_df[openings_df['IsKeyProject'] == 'Y']['ProjectKey'].unique()
non_key_project_list = openings_df[openings_df['IsKeyProject'] == 'N']['ProjectKey'].unique()

# do the processing for key projects
for project in key_project_list:
    project_match_df = process_a_key_project(project)
    final_match_df_for_all_projects = pd.concat([final_match_df_for_all_projects, project_match_df])

# do the processing for non key projects
for project in non_key_project_list:
    project_match_df = process_a_non_key_project(project)
    final_match_df_for_all_projects = pd.concat([final_match_df_for_all_projects, project_match_df])

final_match_df_for_all_projects = final_match_df_for_all_projects.reset_index(drop=True)
final_match_df_for_all_projects['match_id'] = final_match_df_for_all_projects.index

if os.path.isfile(allocation_file):
    os.remove(allocation_file)
if os.path.isfile(unallocated_resources_file):
    os.remove(unallocated_resources_file)
if os.path.isfile(unallocated_openings_file):
    os.remove(unallocated_openings_file)

## Create dataframe for allocated resources
allocated_resources_df = final_match_df_for_all_projects[final_match_df_for_all_projects['is_allocated'] == 'Y'][['project_key', 'opening_id', 'resource_id', 'score', 'project_start_date', 'project_end_date']]

## Create dataframe for unallocated resources
allocated_resources_list = allocated_resources_df['resource_id'].unique().tolist()
unallocated_resource_df = resources_df[(resources_df['EmployeeID'].isin(allocated_resources_list)) == False]['EmployeeID']

## Create dataframe for unallocated openings
allocated_openings_list = allocated_resources_df['opening_id'].unique().tolist()
unallocated_openings_df = openings_df[(openings_df['RequestID'].isin(allocated_openings_list)) == False][['ProjectKey','RequestID']]

final_match_df_for_all_projects.to_csv(allocation_file, header=True, index=False, sep=',')
unallocated_resource_df.to_csv(unallocated_resources_file, header=True, index=False, sep=',')
unallocated_openings_df.to_csv(unallocated_openings_file, header=True, index=False, sep=',')

print ('Total openings fullfilled: ' + str(len(openings_df) - len(unallocated_openings_df.index)))
print ('Total resorces allocated: ' + str(len(resources_df) - len(unallocated_resource_df.index)))
print ("Total Score is : " + str(total_score))
print("Total time taken : %s seconds" % (time.time() - start_time))

print ('************************************************************************************')
print ('***************************** Execution Ends ***************************************')
print ('************************************************************************************')