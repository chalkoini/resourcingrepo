import pandas as pd
import xml.etree.ElementTree as ET
import config
# import time

# start = time.time()
# Location of Resources XML
resources_path = config.resource_file;

# Columns List
column_list = ['Sno', 'EmployeeID', 'EmployeeName', 'DOJ', 'Skills', 'DomainExperience', 'Rating', 'CommunicationsRating', 'NAGP', 'Certifications']
column_list.extend(['YearsOfExperience', 'CurrentRole', 'PreviousCustomerExperience', 'Costperhour', 'AvailableFromDate'])

# Reads the XML file
etree = ET.parse(resources_path)
root = etree.getroot()

total_resources_list = []
for record in root.findall('record'):
    resources_list = []
    try:
        Sno = record.find('Sno').text
    except AttributeError:
        Sno = ''
    resources_list.append(Sno)

    try:
        EmployeeID = record.find('EmployeeID').text
    except AttributeError:
        EmployeeID = ''
    resources_list.append(EmployeeID)
    try:
        EmployeeName = record.find('EmployeeName').text
    except AttributeError:
        EmployeeName = ''
    resources_list.append(EmployeeName)
    try:
        DOJ = record.find('DOJ').text
    except AttributeError:
        DOJ = ''
    resources_list.append(DOJ)
    try:
        Skills = record.find('Skills').text
        skills_fmt = Skills.strip().replace('Expert', '3').replace('Intermediate', '2').replace('Beginner', '1')

        skills_list = skills_fmt.strip().split(',')
        skill_list_final = []
        for skill_item in skills_list:
            skill_split = skill_item.strip().split('-')
            cnt = int(skill_split[1])

            while (cnt >= 1):
                skill_list_final.append(skill_split[0] + '-' + str(cnt))
                cnt -= 1

    except AttributeError:
        skill_list_final = []
    resources_list.append(skill_list_final)

    try:
        DomainExperience = record.find('DomainExperience').text
        domain_list = []
        for domain in DomainExperience.strip().split(','):
            domain_list.append(domain.strip())

    except AttributeError:
        domain_list = []
    resources_list.append(domain_list)

    try:
        Rating = record.find('Rating').text
    except AttributeError:
        Rating = ''
    resources_list.append(Rating)
    try:
        CommunicationsRating = record.find('CommunicationsRating').text
    except AttributeError:
        CommunicationsRating = ''
    resources_list.append(CommunicationsRating)
    try:
        NAGP = record.find('NAGP').text
    except AttributeError:
        NAGP = ''
    resources_list.append(NAGP)
    try:
        Certifications = record.find('Certifications').text
        cert_list = []
        for cert in Certifications.strip().split(','):
            cert_list.append(cert.strip())
    except AttributeError:
        cert_list = []
    resources_list.append(cert_list)
    try:
        YearsOfExperience = record.find('YearsOfExperience').text
    except AttributeError:
        YearsOfExperience = ''
    resources_list.append(YearsOfExperience)
    try:
        CurrentRole = record.find('CurrentRole').text
    except AttributeError:
        CurrentRole = ''
    resources_list.append(CurrentRole)
    try:
        PreviousCustomerExperience = record.find('PreviousCustomerExperience').text
        cust_list = []
        for cust in PreviousCustomerExperience.strip().split(','):
            cust_list.append(cust.strip())
    except AttributeError:
        cust_list = []
    resources_list.append(cust_list)

    try:
        Costperhour = record.find('Costperhour').text
    except AttributeError:
        Costperhour = ''
    resources_list.append(Costperhour)

    try:
       AvailableFromDate = record.find('AvailableFromDate').text
       AvailableFromDate_split = AvailableFromDate.strip().split('-') #Used replace
       AvailableFromDate_fmt = AvailableFromDate_split[2] + AvailableFromDate_split[1] + AvailableFromDate_split[0]
    except AttributeError:
        AvailableFromDate_fmt = ''
       # AvailableFromDate = ''
    resources_list.append(AvailableFromDate_fmt)

    total_resources_list.append(resources_list)

# Loads the list to DataFrame
resources_df = pd.DataFrame(total_resources_list)

# Updates the columns name of DataFrame
resources_df.columns = column_list

resources_df['YearsOfExperience'] = resources_df['YearsOfExperience'].astype(int)

# print("--- %s seconds resources ---" % (time.time() - start))
# print (next(resources_df.iterrows()));

