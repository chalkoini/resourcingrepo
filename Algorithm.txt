1. Point calculation
---------------------------- 

***     1 point - Mandatory skill match and availability match
***   0.3 point - If mtach has NAGP marked 'Y'
***   0.2 point - if match has rating A+
***   0.1 point - if match has rating A
***   0.2 point - if domain matches
***   0.3 point - if domain and client both matches
*** -0.05 point - subtract 0.05 multlipied by no of exp over the exp required


2. Restrictions
-----------------------------
*** Key Project/Key Position : max points 2
*** Key Project/non - Key Position : max points 1.3
*** non - Key Project/Key Position : max points 1.5
*** non - Key Project/non - Key Position : max points 1.5

*** max average points to be given - 1.5 (even if 6 key resource required for key project, max points to be given 1.5 * 6 = 9)

*** Final score - sum of points for all matches for all projects


3. Algo
------------------------------
STEP 1: Read both resources & opening xml files in pandas dataframes let's say resources_df & openings_df respectively.

STEP 2: Iterate over resources_df and for each row in resources_df, calculate it's score against each opening in openings_df, using below:
	 
	 For a resource R1 in resources_df,
	 
	 2.1:  get all openings from openings_df having ProjectStartDate(openings_df) > AvailableFromDate(resources_df) 
	 												& MandatorySkills(openings_df) in Skills(resources_df) in dataframe relevant_openings_df.
	 	   (NOTE 1 : Skills can be a list as well, for e.g. Project require only .Net but resource has both .Net & SQL Server in skills)
	 	   (NOTE 2 : Expert candidate can be matched with intermediate, beginner opening but not vice versa and like wise for Intermediate with beginner)

	 2.2:  create an empty dataframe with columns mentioned below:

	 			resource_match_df -> columns are 
	 									match_id    : match id for the match
	 									resource_id : resource id from resources_df,
	 									opening_id  : opening id from openings_df,
	 									ProjectKey  : project code from openings_df,
	 									score		: calculated score,
	 									days_unused : diff in resource availability & project start date,
	 									key_project : flag with Y or N,
	 									key_position: flag with Y or N,
	 									ProjectStartDate : project start date from openings_df,
	 									ProjectEndDate : project end date from openings_df,
	 									OptionalSkills : optional skill from openings_df,
	 									Skills : skills of candidate from resources_df,
	 									ClientCommunication : client communication from openings_df,
	 									CommunicationsRating : communication rating from resources_df,
	 									CertificationRequirement : certification required bool field from openings_df,
	 									Certifications : certification list of candidate from resources_df,
	 									opening_role : role from openings_df,
	 									resource_role : role of resource from resources_df,
	 									Costperhour : cost per hour of candidate from resources_df,
	 									allocated : flag to specify if the candidate is allocated to some project

	 2.3   create a dictionary (project_openings) as below which will have projects and their respective openings specified as key or non-key (will be used later in STEP 3 to process
	  data for each project) 

	  			project_openings_dict = {
	  								key_projects : {
	  												KP1 : [KP1O1, KP1O2, ..],
	  												KP2 : [KP2O1, KP2O2, ..]
	  												...
	  											   }
	  								non_key_projects : {
		  												NKP1 : [NKP1O1, NKP1O2, ..],
		  												NKP2 : [NKP2O1, NKP2O2, ..]
		  												...
		  											   }
	  								}



	 2.4:  Iterate over relevant_openings_df for resource R1, to calculate score for each as below:

	 	For Opening O1 in relevant_openings_df,

	 	2.4.1: Calculate score of resource R1 for opening O1, resource_score = 1 (since opening already matched in step 2.1) + 
	 																 0.3 (if R1 has field 'NAGP' as Y) + 
	 																 0.2 (if R1 has field 'Rating' as A+) or 0.1 (if R1 has field 'Rating' as A) +
	 																 0.3 (if R1 has field PreviousCustomerExperience = CustomerName of O1) +
	 																 0.2 (if R1 has field DomainExperience = DomainExperience of O1) -
	 																 0.05 * (YearsOfExperience of R1 - YearsOfExperience of O1) if 
	 																 		(YearsOfExperience of R1 > YearsOfExperience of O1)

	 	2.4.2 Check if a valid match based on below matrix, and set is_valid flag accordingly : 

	 			*** Key Project/Key Position : max points 2
				*** Key Project/non - Key Position : max points 1.3
				*** non - Key Project/Key Position : max points 1.5
				*** non - Key Project/non - Key Position : max points 1.5

	 	2.4.2 create row in resource_match_df with data as below as below:

	 			resource_match_df = {
	 									match_id    : generated match id,
	 									resource_id : Sno of R1,
	 									opening_id  : RequestID of O1,
	 									ProjectKey  : project code from openings_df,
	 									score		: resource_score,
	 									days_unused : ProjectStartDate of O1 - AvailableFromDate of R1,
	 									key_project : IsKeyProject of O1,
	 									key_position: IsKeyPosition of O1,
	 									ProjectStartDate : ProjectStartDate of O1,
	 									ProjectEndDate : ProjectEndDate of O1,
	 									OptionalSkills : OptionalSkills of O1,
	 									Skills : Skills of R1,
	 									ClientCommunication : ClientCommunication of O1,
	 									CommunicationsRating : CommunicationsRating of R1,
	 									CertificationRequirement : CertificationRequirement of O1,
	 									Certifications : Certifications of R1,
	 									opening_role : Role of O1,
	 									resource_role : Role of R1,
	 									Costperhour : Costperhour of R1,
	 									is_valid_match : is_valid, 
	 									allocated : null (will be updated once the resource is allocated to some project)
	 								}

	 	2.4.3 populate project_openings_dict according to IsKeyProject and IsKeyPosition in openings_df

	 	Repeat for all openings in openings_df.

	 Repeat for all resources in resources_df.

At the end of Step 2, we will have a dataframe, resource_match_df, with score of all matched resources, against all matched openings.

STEP 3: Iterate over key projects i.e. project_openings_dict['key_projects'] do the below:

	Set variable overall_score = 0
		
	For a project key KP1, in project_openings_dict['key_projects'],

	3.1 create a blank list as below:

		openings_match_list = [
									[{match_id:score},{match_id:score},{match_id:score},....],
									[{match_id:score},{match_id:score},{match_id:score},....],
									[{match_id:score},{match_id:score},{match_id:score},....],
									...........
								]

		This is a list having lists of matches for each opening. Each item in a list is the list of matches for that opening.

	3.1 Set variable project_score = 0;
	
	3.2 for an opening in KP1, let's say KP1O1

	3.3 select rows from resource_match_df where ProjectKey == KP1 & (allocated == 'N' or ProjectEndDate < ProjectStartDate of KP1) & opening_id = KP1O1 & is_valid_match = 'Y' 
												sorted by score in current_project_opening_matches.

	3.4 for each row in current_project_opening_matches populate openings_match_list

		Example list would be : [[{1:1},{2:1.5},{3:2}],[{4:1},{5:1.5},{6:2}],[{7:1},{8:1.5},{9:2},{10:2}]] where 1,2,3 are the match id for first opening in the 
								project and 1, 1.5 & 2 are their respective scores and so on.

	3.5 create a combination of each match item for e.g. {1:1} with matche items in other openings i.e. with {4,1} & {7,1} using below sample code:

		import itertools
		dict_list = [[{1:1},{2:2},{3:3}],[{4:4},{5:5},{6:6}],[{7:7},{8:8},{9:9},{10:10}]]
		combinations = list(itertools.product(*dict_list));
		print(combinations)

		This will create a list of combinations as below:

			project_match_combinations = 

			[({1: 1}, {4: 4}, {7: 7}), ({1: 1}, {4: 4}, {8: 8}), ({1: 1}, {4: 4}, {9: 9}), ({1: 1}, {4: 4}, {10: 10}),
			[({({1: 1}, {5: 5}, {7: 7}), ({1: 1}, {5: 5}, {8: 8}), ({1: 1}, {5: 5}, {9: 9}), ({1: 1}, {5: 5}, {10: 10}),
			[({({1: 1}, {6: 6}, {7: 7}), ({1: 1}, {6: 6}, {8: 8}), ({1: 1}, {6: 6}, {9: 9}), ({1: 1}, {6: 6}, {10: 10}),
			[({({2: 2}, {4: 4}, {7: 7}), ({2: 2}, {4: 4}, {8: 8}), ({2: 2}, {4: 4}, {9: 9}), ({2: 2}, {4: 4}, {10: 10}),
			[({({2: 2}, {5: 5}, {7: 7}), ({2: 2}, {5: 5}, {8: 8}), ({2: 2}, {5: 5}, {9: 9}), ({2: 2}, {5: 5}, {10: 10}),
			[({({2: 2}, {6: 6}, {7: 7}), ({2: 2}, {6: 6}, {8: 8}), ({2: 2}, {6: 6}, {9: 9}), ({2: 2}, {6: 6}, {10: 10}),
			[({({3: 3}, {4: 4}, {7: 7}), ({3: 3}, {4: 4}, {8: 8}), ({3: 3}, {4: 4}, {9: 9}), ({3: 3}, {4: 4}, {10: 10}),
			[({({3: 3}, {5: 5}, {7: 7}), ({3: 3}, {5: 5}, {8: 8}), ({3: 3}, {5: 5}, {9: 9}), ({3: 3}, {5: 5}, {10: 10}),
			[({({3: 3}, {6: 6}, {7: 7}), ({3: 3}, {6: 6}, {8: 8}), ({3: 3}, {6: 6}, {9: 9}), ({3: 3}, {6: 6}, {10: 10})]


	3.6 for each combination in project_match_combinations, get the sum of scores and check if it is below 1.5,

		3.6.1 if yes, break the loop and update match_id of the current combination in resource_match_df setting allocated as 'Y'.

		3.6.2 if no, keep on iterating over project_match_combinations, and do 3.6.1 if sum is below 1.5

	3.7 once we break out of above loop, update project_score = sum of scores of the combination

	3.8 set overall_score = overall_score + project_score

once out of this loop we will have overall_score and resource_match_df with allocated resources.


		 

