import pandas as pd
import time
import os
import itertools

##----------------------------------------------------------------------------------------##
##------------------------------  Get Openings/Resources DF  -----------------------------##
##----------------------------------------------------------------------------------------##
from opening_xml_parser import openings_df
from resources_xml_parser import resources_df

## Start timer for execution time check
start_time = time.time()

## Current folder
current_folder = os.getcwd()
print (current_folder)
output_file = current_folder + "\\FinalOutput.csv"
print (output_file)

## config properties
pd.options.mode.chained_assignment = None

##----------------------------------------------------------------------------------------##
##---------------------------------  Final Match DF columns  -----------------------------##
##----------------------------------------------------------------------------------------##
match_df_column_list = ['match_id', 'resource_id', 'opening_id', 'project_key', 'score', 'key_project',
                        'key_position', 'project_start_date', 'project_end_date', 'is_allocated']

##----------------------------------------------------------------------------------------##
##---------------------------------  Function Declarations  ------------------------------##
##----------------------------------------------------------------------------------------##
## Function to start processing on a key project - match and allocate resources
def process_a_key_project(project):
    project_match_df = get_project_match_and_score(project)

    is_balancing_required = check_resource_balancing(project_match_df)

    return project_match_df

## Function to start processing on a non-key project - match and allocate resources
def process_a_non_key_project(project):
    project_match_df = get_project_match_and_score(project)
    # project_match_df.sort_values('score', inplace=True, ascending=False)

    project_match_df = direct_project_allocation(project_match_df)

    return project_match_df

## get all matches and scores for a project
def get_project_match_and_score(project):
    is_key_project = openings_df[openings_df['ProjectKey'] == project]['IsKeyProject'].unique()[0]
    project_match_df = pd.DataFrame(data=None, columns=None, index=None)
    for index, opening in openings_df[openings_df['ProjectKey'] == project].iterrows():

        resource_match_df = resources_df[(resources_df['AvailableFromDate'] <= opening['RequestStartDate']) &
                                         (resources_df['Skills'].apply(
                                             lambda x: set(x) >= set(opening['MandatorySkills'])))]

        if not resource_match_df.empty:
            # Calculate Score for all resources against an opening
            resource_match_df = calculate_score_of_resource(resource_match_df, opening)

            # Validate the score calculated
            if ((is_key_project == 'Y') & (opening['IsKeyPosition'] == 'N')):
                resource_match_df = resource_match_df[resource_match_df['score'] <= 1.3]
            elif (is_key_project == 'N') & (opening['IsKeyPosition'] == 'Y'):
                resource_match_df = resource_match_df[resource_match_df['score'] <= 1.5]
            elif (is_key_project == 'N') & (opening['IsKeyPosition'] == 'N'):
                resource_match_df = resource_match_df[resource_match_df['score'] <= 1]


            final_opening_valid_match_df = populate_df_for_resources(resource_match_df, opening)

            project_match_df = pd.concat([project_match_df, final_opening_valid_match_df])

    project_match_df = project_match_df.reset_index(drop=True)
    project_match_df['match_id'] = project_match_df.index

    return project_match_df

## Function to calculate score of the resource against matched openings in function match_resource_with_openings_df_PR
def calculate_score_of_resource(resource_match_df, opening):
    resource_match_df['score'] = 1

    DomainExperienceMatched = resource_match_df['DomainExperience'].apply(lambda x: opening['ProjectDomain'] in set(x))
    CustomerMatched = resource_match_df['PreviousCustomerExperience'].apply(lambda x: opening['CustomerName'] in set(x))

    DomainExperienceMatchedScore = DomainExperienceMatched.apply(lambda x: 0.2 if (x) else 0)
    CustomerMatchedScore = CustomerMatched.apply(lambda x: 0.3 if (x) else 0)
    resource_match_df['score'] += DomainExperienceMatchedScore + CustomerMatchedScore

    # Score Calculation   for Years of work experience
    workex_diff = opening['YearsOfExperience'] - resource_match_df['YearsOfExperience']
    workex_score = workex_diff.apply(lambda x: x * 0.05 if (x < 0) else 0)
    resource_match_df['score'] += workex_score

    nagp_score = resource_match_df['NAGP'].apply(lambda x: 0.3 if (x == 'Y') else 0)
    rating_score = resource_match_df['Rating'].apply(lambda x: 0.2 if (x == 'A+') else (0.1 if (x == 'A') else 0))
    resource_match_df['score'] += nagp_score + rating_score

    return resource_match_df

## Function to populate a match dataframe for all resources against an opening
def populate_df_for_resources(resource_match_df, opening):
    matched_records_for_opening = pd.DataFrame(data=None, columns= match_df_column_list, index=None)

    matched_records_for_opening['score'] = resource_match_df['score']
    matched_records_for_opening['key_project'] = opening['IsKeyProject']
    matched_records_for_opening['key_position'] = opening['IsKeyPosition']
    matched_records_for_opening['project_start_date'] = opening['RequestStartDate']
    matched_records_for_opening['project_end_date'] = opening['ProjectEndDate']
    matched_records_for_opening['is_allocated'] = 'N'
    matched_records_for_opening['resource_id'] = resource_match_df['EmployeeID']
    matched_records_for_opening['opening_id'] = opening['RequestID']
    matched_records_for_opening['match_id'] = resource_match_df.index
    matched_records_for_opening['project_key'] = opening['ProjectKey']

    return matched_records_for_opening

## Check if balancing is required in a key project
def check_resource_balancing(project_match_df):
    is_balancing_required = True
    openings_matched_list = project_match_df['opening_id'].unique()
    kp_nkp_df = (openings_df[openings_df['RequestID'].isin(openings_matched_list)]['IsKeyPosition']).to_frame()
    kp_cnt = ((kp_nkp_df[kp_nkp_df['IsKeyPosition'] == 'Y']['IsKeyPosition']).value_counts()).tolist()
    nkp_cnt = ((kp_nkp_df[kp_nkp_df['IsKeyPosition'] == 'N']['IsKeyPosition']).value_counts()).tolist()

    if kp_cnt == []:
        kp_cnt = 0
    else:
        kp_cnt = kp_cnt[0]

    if nkp_cnt == []:
        nkp_cnt = 0
    else:
        nkp_cnt = nkp_cnt[0]

    if nkp_cnt >= (3 * kp_cnt):
        is_balancing_required = False

    return is_balancing_required

## Allocated a resource in a project
def allocate_resource(match_id, project_match_df):
    project_match_df.loc[(project_match_df['match_id'] == match_id), 'is_allocated'] = 'Y'
    matched_rows = project_match_df[project_match_df['match_id'] == match_id]
    resource_dict = dict(zip(matched_rows['resource_id'], matched_rows['project_end_date']))
    for key, value in resource_dict.items():
        project_match_df.loc[((project_match_df['resource_id'] == key) &
                              (project_match_df['is_allocated'] != 'Y')), 'is_allocated'] = 'AA'

    return project_match_df


def direct_project_allocation(project_match_df):
    project_score = 0

    matched_openings_dict = (project_match_df['opening_id'].value_counts().to_dict())
    matched_openings_list = sorted(matched_openings_dict, key=matched_openings_dict.get)
    for opening in matched_openings_list:
        selected_df_row = project_match_df[(project_match_df['is_allocated'] == 'N') & (project_match_df['opening_id'] == opening)]
        if not selected_df_row.empty:
            selected_df_row = selected_df_row.ix[selected_df_row['score'].idxmax()]
            project_score += selected_df_row['score']
            match_id = selected_df_row['match_id']
            project_match_df = allocate_resource(match_id, project_match_df)

    return project_match_df


##----------------------------------------------------------------------------------------##
##---------------------------  Processing starts from here  ------------------------------##
##----------------------------------------------------------------------------------------##
final_match_df_for_all_projects = pd.DataFrame(data=None, columns=match_df_column_list, index=None)
total_score = 0

# Sort openings on request start date so that openings with earlier start date are allocated first
openings_df.sort_values(['RequestStartDate'], inplace=True, ascending=True)

# Create list of key and non key projects
key_project_list = openings_df[openings_df['IsKeyProject'] == 'Y']['ProjectKey'].unique()
non_key_project_list = openings_df[openings_df['IsKeyProject'] == 'N']['ProjectKey'].unique()

# do the processing for key projects
for project in key_project_list:
    project_match_df = process_a_key_project(project)
    final_match_df_for_all_projects = pd.concat([final_match_df_for_all_projects, project_match_df])

# do the processing for non key projects
for project in non_key_project_list:
    project_match_df = process_a_non_key_project(project)
    final_match_df_for_all_projects = pd.concat([final_match_df_for_all_projects, project_match_df])

final_match_df_for_all_projects = final_match_df_for_all_projects.reset_index(drop=True)
final_match_df_for_all_projects['match_id'] = final_match_df_for_all_projects.index

if os.path.isfile(output_file):
    os.remove(output_file)

final_match_df_for_all_projects.to_csv(output_file, header=True, index=False, sep=',')
print ("Total Score is : " + str(total_score))
print("--- %s seconds ---" % (time.time() - start_time))