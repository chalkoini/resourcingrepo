import pandas as pd
import time
import os
import itertools
import numpy as np
##----------------------------------------------------------------------------------------##
##------------------------------  Get Openings/Resources DF  -----------------------------##
##----------------------------------------------------------------------------------------##
from opening_xml_parser import openings_df
from resources_xml_parser import resources_df

## Start timer for execution time check
start_time = time.time()

## Current folder
current_folder = os.getcwd()
print (current_folder)
output_file = current_folder + "\\FinalOutput.csv"
print (output_file)

## config properties
pd.options.mode.chained_assignment = None

##----------------------------------------------------------------------------------------##
##---------------------------------  Final Match DF columns  -----------------------------##
##----------------------------------------------------------------------------------------##
match_df_column_list = ['match_id', 'resource_id', 'opening_id', 'project_key', 'score', 'days_unused', 'key_project',
                        'key_position', 'project_start_date', 'project_end_date', 'is_valid_match', 'waste_skill_cnt', 'is_allocated']

##----------------------------------------------------------------------------------------##
##---------------------------------  Function Declarations  ------------------------------##
##----------------------------------------------------------------------------------------##

## Function to match a resource with openings in openings_df
def match_resource_with_openings_df(resource):
    initial_match_df = openings_df[(openings_df['RequestStartDate'] >= resource['AvailableFromDate']) & (
        openings_df['MandatorySkills'].apply(lambda x: set(x) <= set(resource['Skills'])))]

    final_matches_for_resource = None

    if not initial_match_df.empty:
        # Calculate Score
        total_score = calculateScoreOfResource(initial_match_df, resource)
        # Add Total Score column to matched DF
        initial_match_df['TotalScore'] = total_score  # Added Column for each resource score against opening
        # Validate the matched openings
        validated_match_df = validateResourceScores(initial_match_df)
        # Populate final DF with validated matched openings for a resource
        final_matches_for_resource = populateDataFrameForResource(validated_match_df, resource['EmployeeID'])

    return final_matches_for_resource

## Function to calculate score of the resource against matched openings in function match_resource_with_openings_df_PR
def calculateScoreOfResource(initial_match_df, resource):
    DomainExperienceMatched = initial_match_df['ProjectDomain'].isin(
        resource['DomainExperience'])  # Domain experience Match
    CustomerMatched = initial_match_df['CustomerName'].isin(resource['PreviousCustomerExperience'])  # Customer Matching
    DomainExperienceMatchedScore = DomainExperienceMatched.apply(lambda x: 0.2 if (x) else 0)
    CustomerMatchedScore = CustomerMatched.apply(lambda x: 0.3 if (x) else 0)

    # Score Calculation   for Years of work experience
    workex_score = initial_match_df['YearsOfExperience'] - resource['YearsOfExperience']
    workex_score = workex_score.apply(lambda x: x * 0.05 if (x < 0) else 0)

    # Score Calculation
    score = 1
    if resource['NAGP'] == 'Y':
        score += 0.3
    if resource['Rating'] == 'A+':
        score += 0.2
    elif resource['Rating'] == 'A':
        score += 0.1

    total_score = score + workex_score + DomainExperienceMatchedScore + CustomerMatchedScore
    # total_score = np.round(total_score,decimals=2)


    return total_score

## Function to validate the score calculated for a resource against matched openings for project/position matrix in function calculateScoreOfResource
def validateResourceScores(initial_match_df):
    validated_match_df_flag = initial_match_df.apply(lambda x: 'Y' if (x['IsKeyProject'] == 'Y' and x['IsKeyPosition'] == 'Y' and x['TotalScore'] <= 2)
                                                else ('Y' if (x['IsKeyProject'] == 'Y' and x['IsKeyPosition'] == 'N' and x['TotalScore'] <= 1.3)
                                                else ('Y' if (x['IsKeyProject'] == 'N' and x['IsKeyPosition'] == 'Y' and x['TotalScore'] <= 1.5)
                                                else ('Y' if (x['IsKeyProject'] == 'N' and x['IsKeyPosition'] == 'Y' and x['TotalScore'] <= 1)
                                                else 'N'))), axis=1)

    initial_match_df['is_valid_match'] = validated_match_df_flag
    initial_match_df = initial_match_df[initial_match_df['is_valid_match'] == 'Y']

    return initial_match_df

## Function to populate a match dataframe for the resource against all validate matched openings
def populateDataFrameForResource(validated_match_df, resource_id):
    # print (resource_id)

    # print('#########################')
    matched_records_for_resource = pd.DataFrame(data=None, columns= match_df_column_list, index=None)
    matched_records_for_resource['match_id'] = None
    matched_records_for_resource['opening_id'] = validated_match_df['RequestID']
    matched_records_for_resource['project_key'] = validated_match_df['ProjectKey']
    matched_records_for_resource['score'] = validated_match_df['TotalScore']
    matched_records_for_resource['days_unused'] = None # Login not written
    matched_records_for_resource['key_project'] = validated_match_df['IsKeyProject']
    matched_records_for_resource['key_position'] = validated_match_df['IsKeyPosition']
    matched_records_for_resource['project_start_date'] = validated_match_df['RequestStartDate']
    matched_records_for_resource['project_end_date'] = validated_match_df['ProjectEndDate']
    matched_records_for_resource['is_valid_match'] = validated_match_df['is_valid_match']
    matched_records_for_resource['waste_skill_cnt'] = None # Login not written
    matched_records_for_resource['is_allocated'] = 'N'
    matched_records_for_resource['resource_id'] = resource_id

    return matched_records_for_resource

def resource_opening_match(final_match_df_for_all_openings_resources, total_score):
    final_match_df_for_all_openings_resources.sort_values(['project_start_date'], inplace=True, ascending=True)
    for projects in final_match_df_for_all_openings_resources['project_key'].unique():
        opening_resource_df = final_match_df_for_all_openings_resources[final_match_df_for_all_openings_resources['project_key'] == projects]

        all_openings_match_list = []
        for opening in opening_resource_df['opening_id'].unique():
            openings_match_list = []
            opening_resource_sorted_df = opening_resource_df[opening_resource_df['opening_id'] == opening]
            opening_resource_sorted_df.sort_values(['score'], inplace=True, ascending=False)

            for index, match in opening_resource_sorted_df.iterrows():
                openings_match = {match['match_id'] : match['score']}
                openings_match_list.append(openings_match)
            all_openings_match_list.append(openings_match_list)

            selected_match_id_list, total_score = project_match_combinations(all_openings_match_list, total_score)

    return selected_match_id_list, total_score

def project_match_combinations(all_openings_match_list, total_score):
    combinations = list(itertools.product(*all_openings_match_list));
    selected_match_id_list = []
    for comb in combinations:
        current_score = 0
        for item in comb:
            selected_match_id_list.append(list(item.keys())[0])
            current_score += list(item.values())[0]
        comb_score =  round((current_score/ len(comb)),2)
        if comb_score <= 1.5:
            print (comb_score)
            total_score += comb_score
            break

    return selected_match_id_list, total_score

def allocate_resources(selected_match_id_list, final_match_df_for_all_openings_resources):
    final_match_df_for_all_openings_resources.loc[final_match_df_for_all_openings_resources['match_id'].isin(selected_match_id_list), 'is_allocated'] = 'Y'
    matched_rows = final_match_df_for_all_openings_resources[final_match_df_for_all_openings_resources['match_id'].isin(selected_match_id_list)]
    resource_dict = dict(zip(matched_rows['resource_id'], matched_rows['project_end_date']))
    for key, value in resource_dict.items():
        final_match_df_for_all_openings_resources.loc[((final_match_df_for_all_openings_resources['resource_id'] == key) &
                                                       (final_match_df_for_all_openings_resources['project_start_date'] > value)), 'is_allocated'] = 'N'
        final_match_df_for_all_openings_resources.loc[((final_match_df_for_all_openings_resources['resource_id'] == key) &
                                                        (final_match_df_for_all_openings_resources['project_start_date'] <= value)), 'is_allocated'] = 'Y'

##----------------------------------------------------------------------------------------##
##---------------------------  Processing starts from here  ------------------------------##
##----------------------------------------------------------------------------------------##
final_match_df_for_all_openings_resources = pd.DataFrame(data=None, columns=match_df_column_list, index=None)
total_score = 0

for index, resource in resources_df.iterrows():
    resource_match_df = match_resource_with_openings_df(resource)
    final_match_df_for_all_openings_resources = pd.concat([final_match_df_for_all_openings_resources, resource_match_df])

final_match_df_for_all_openings_resources = final_match_df_for_all_openings_resources.reset_index(drop=True)
final_match_df_for_all_openings_resources['match_id'] = final_match_df_for_all_openings_resources.index

selected_match_id_list, total_score = resource_opening_match(final_match_df_for_all_openings_resources, total_score)
#selected_match_id_list, total_score = project_match_combinations(all_openings_match_list, total_score)
allocate_resources(selected_match_id_list, final_match_df_for_all_openings_resources)

if os.path.isfile(output_file):
    os.remove(output_file)

final_match_df_for_all_openings_resources.to_csv(output_file, header=True, index=False,sep=',')
print ("Total Score is : " + str(total_score))
print("--- %s seconds ---" % (time.time() - start_time))
