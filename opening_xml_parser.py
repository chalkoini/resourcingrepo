import pandas as pd
import xml.etree.ElementTree as ET
import config

# Location of Openings XML
openings_path = config.openings_file;

# Columns List
column_list = ['RequestID', 'ClientKey', 'ProjectKey', 'CustomerName', 'ProjectName','IsKeyProject', 'ProjectDomain', 'ProjectStartDate', 'ProjectEndDate', 'Role']
column_list.extend(['IsKeyPosition', 'YearsOfExperience', 'MandatorySkills', 'OptionalSkills', 'DomainExperience', 'ClientCommunication', 'CertificationRequirement'])
column_list.extend(['RequestStartDate', 'AllocationStartDate', 'AllocationEndDate', 'BillingRate', 'BillingAllocation'])

# Reads the XML file
etree = ET.parse(openings_path)
root = etree.getroot()

total_openings_list = []
for record in root.findall('record'):
    openings_list = []
    try:
        RequestID = record.find('RequestID').text
    except AttributeError:
        RequestID = ''
    openings_list.append(RequestID)

    try:
        ClientKey = record.find('ClientKey').text
    except AttributeError:
        ClientKey = ''
    openings_list.append(ClientKey)
    try:
        ProjectKey = record.find('ProjectKey').text
    except AttributeError:
        ProjectKey = ''
    openings_list.append(ProjectKey)
    try:
        CustomerName = record.find('CustomerName').text
    except AttributeError:
        CustomerName = ''
    openings_list.append(CustomerName)
    try:
        ProjectName = record.find('ProjectName').text
    except AttributeError:
        ProjectName = ''
    openings_list.append(ProjectName)
    try:
        IsKeyProject = record.find('IsKeyProject').text
    except AttributeError:
        IsKeyProject = ''
    openings_list.append(IsKeyProject)
    try:
        ProjectDomain = record.find('ProjectDomain').text
    except AttributeError:
        ProjectDomain = ''
    openings_list.append(ProjectDomain)
    try:
        ProjectStartDate = record.find('ProjectStartDate').text
        ProjectStartDate_split = ProjectStartDate.strip().split('-')
        ProjectStartDate_fmt = ProjectStartDate_split[2] + ProjectStartDate_split[1] + ProjectStartDate_split[0]
    except AttributeError:
        ProjectStartDate_fmt = ''
    openings_list.append(ProjectStartDate_fmt)
    try:
        ProjectEndDate = record.find('ProjectEndDate').text
        ProjectEndDate_split = ProjectEndDate.strip().split('-')
        ProjectEndDate_fmt = ProjectEndDate_split[2] + ProjectEndDate_split[1] + ProjectEndDate_split[0]
    except AttributeError:
        ProjectEndDate_fmt = ''
    openings_list.append(ProjectEndDate_fmt)
    try:
        Role = record.find('Role').text
    except AttributeError:
        Role = ''
    openings_list.append(Role)
    try:
        IsKeyPosition = record.find('IsKeyPosition').text
    except AttributeError:
        IsKeyPosition = ''
    openings_list.append(IsKeyPosition)
    try:
        YearsOfExperience = record.find('YearsOfExperience').text
    except AttributeError:
        YearsOfExperience = ''
    openings_list.append(YearsOfExperience)
    try:
        MandatorySkills = record.find('MandatorySkills').text

        skills_fmt = MandatorySkills.strip().replace('Expert', '3').replace('Intermediate', '2').replace('Beginner', '1')
        skill_list = []
        skills_fmt_split = skills_fmt.strip().split(',')
        for item in skills_fmt_split:
            skill_list.append(str(item.strip()))

    except AttributeError:
        skill_list = []
    openings_list.append(skill_list)
    try:
        OptionalSkills = record.find('OptionalSkills').text
    except AttributeError:
        OptionalSkills = ''
    openings_list.append(OptionalSkills)
    try:
        DomainExperience = record.find('DomainExperience').text
    except AttributeError:
        DomainExperience = ''
    openings_list.append(DomainExperience)
    try:
        ClientCommunication = record.find('ClientCommunication').text
    except AttributeError:
        ClientCommunication = ''
    openings_list.append(ClientCommunication)
    try:
        CertificationRequirement = record.find('CertificationRequirement').text
    except AttributeError:
        CertificationRequirement = ''
    openings_list.append(CertificationRequirement)
    try:
        RequestStartDate = record.find('RequestStartDate').text
        RequestStartDate_split = RequestStartDate.strip().split('-')
        RequestStartDate_fmt = RequestStartDate_split[2] + RequestStartDate_split[1] + RequestStartDate_split[0]
    except AttributeError:
        RequestStartDate_fmt = ''
    openings_list.append(RequestStartDate_fmt)
    try:
        AllocationStartDate = record.find('AllocationStartDate').text
    except AttributeError:
        AllocationStartDate = ''
    openings_list.append(AllocationStartDate)
    try:
        AllocationEndDate = record.find('AllocationEndDate').text
        AllocationEndDate_split = AllocationEndDate.strip().split('-')
        AllocationEndDate_fmt = AllocationEndDate_split[2] + AllocationEndDate_split[1] + AllocationEndDate_split[0]
    except AttributeError:
        AllocationEndDate_fmt = ''
    openings_list.append(AllocationEndDate_fmt)
    try:
        BillingRate = record.find('BillingRate').text
    except AttributeError:
        BillingRate = ''
    openings_list.append(BillingRate)
    try:
        BillingAllocation = record.find('BillingAllocation').text
    except AttributeError:
        BillingAllocation = ''
    openings_list.append(BillingAllocation)

    total_openings_list.append(openings_list)

# Loads the list to DataFrame
openings_df = pd.DataFrame(total_openings_list)

# Updates the columns name of DataFrame
openings_df.columns = column_list

openings_df['YearsOfExperience'] = openings_df['YearsOfExperience'].astype(int)
#openings_df.ProjectDomain = openings_df.ProjectDomain.astype('category')
#openings_df.ProjectName = openings_df.ProjectName.astype('category')
# print(openings_df['YearsOfExperience'])

# print(openings_df.dtypes)
#
# openings_df[['MandatorySkills']] = openings_df[['MandatorySkills']].tolist()
#
# print (openings_df.dtypes)

# print (next(openings_df.iterrows()));