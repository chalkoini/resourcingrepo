import pandas as pd
import xml.etree.ElementTree as ET
#First Code
resource_path = 'C:\\Users\\pankajrathi\\Desktop\\Code\\resources.xml'

etree = ET.parse(resource_path)
root = etree.getroot()
for record in root.findall('record'):
    EmployeeID = record.find('EmployeeID').text
    DOJ = record.find('DOJ').text
    Skills = record.find('Skills').text
    try:
        DomainExperience = record.find('DomainExperience').text
    except AttributeError:
        DomainExperience = '*****'
    Rating = record.find('Rating').text
    CommunicationsRating = record.find('CommunicationsRating').text
    NAGP = record.find('NAGP').text
    YearsOfExperience = record.find('YearsOfExperience').text
    try:
        CurrentRole = record.find('CurrentRole').text
    except AttributeError:
        CurrentRole = '*****'
    try:
        PreviousCustomerExperience = record.find('PreviousCustomerExperience').text
    except AttributeError:
        PreviousCustomerExperience = '*****'
    AvailableFromDate = record.find('AvailableFromDate').text

    print (EmployeeID, DOJ, Skills, DomainExperience, Rating, CommunicationsRating, NAGP, YearsOfExperience, CurrentRole, PreviousCustomerExperience, AvailableFromDate)


